FROM maven:3.6.0-jdk-11 AS build

COPY . /home/app/src

COPY pom.xml /home/app

RUN mvn -f /home/app/pom.xml clean package

FROM tomcat:8.5.43-jdk8

COPY --from=build /home/app/src/target/onlinebookstore-0.0.1-SNAPSHOT.war  /usr/local/tomcat/webapps/

